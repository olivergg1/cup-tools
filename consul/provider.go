package consul

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/hashicorp/consul/api"
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	c.Set("consul.client", func(c cup.Container) interface{} {
		dc := c.MustGet("sd.datacenter").(string)

		config := api.DefaultConfig()
		config.Datacenter = dc

		client, err := api.NewClient(config)
		if err != nil {
			panic(err)
		}

		return client
	})
}

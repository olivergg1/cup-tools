package consul

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/hashicorp/consul/api"
	"github.com/stretchr/testify/suite"
	"testing"
)

type ProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider  *Provider
}

func (s *ProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()
	s.container.Set("sd.datacenter", "dc1")
	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegister() {
	s.provider.Register(s.container)

	client, err := s.container.Get("consul.client")
	s.Require().NoError(err)
	s.Require().IsType((*api.Client)(nil), client)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}

package logger

import (
	"github.com/sirupsen/logrus"
	"os"
	"strings"
	"bitbucket.org/kovalevm/cup"
)

const levelEnvName = "LOG_LEVEL"
const defaultLogLevel = logrus.InfoLevel

const formatEnvName = "LOG_FORMAT"

type Provider struct{}

func (p *Provider) Register(container cup.Container) {
	p.registerLogLevel(container)
	p.registerLogFormat(container)

	container.Set("logger", func(c cup.Container) interface{} {
		level := c.MustGet("logger.level").(logrus.Level)
		formatter := c.MustGet("logger.format").(logrus.Formatter)

		logger := logrus.New()
		logger.Formatter = formatter
		logger.SetLevel(level)
		return logger
	})
}

func (p *Provider) registerLogLevel(c cup.Container) {
	c.Set("logger.level", func(c cup.Container) interface{} {
		env, exist := os.LookupEnv(levelEnvName)

		if exist && env != "" {
			lvl, err := logrus.ParseLevel(env)
			if err != nil {
				// we can't use c.MustGet("logger") here
				logrus.Errorf(
					"Failed parse '%v' env variable. Value - %v. Error - %v",
					levelEnvName,
					env,
					err,
				)
			} else {
				return lvl
			}
		}

		return defaultLogLevel
	})
}

func (p *Provider) registerLogFormat(c cup.Container) {
	c.Set("logger.format", func(c cup.Container) interface{} {
		defaultFormatter := &logrus.TextFormatter{}

		format, exist := os.LookupEnv(formatEnvName)

		if exist && format != "" {
			format = strings.ToLower(format)

			switch format {
			case "json":
				return &logrus.JSONFormatter{}

			case "text":

			default:
				logrus.Errorf(
					"Failed parse '%v' env variable: '%v'. Allowed options: 'text','json'. Use 'text' as default",
					formatEnvName,
					format,
				)
			}
		}

		return defaultFormatter
	})
}

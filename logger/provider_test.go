package logger

import (
	"github.com/stretchr/testify/suite"
	"github.com/sirupsen/logrus"
	"testing"
	"bitbucket.org/kovalevm/cup"
	"os"
)

type LoggerProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider *Provider
}

func (s *LoggerProviderTestSuite) SetupTest()  {
	s.container = cup.NewApp()
	s.provider = new(Provider)
}

func (s *LoggerProviderTestSuite) TestRegisterLevel() {
	s.provider.Register(s.container)

	level, err := s.container.Get("logger.level")
	s.Require().NoError(err)
	s.Require().Equal(defaultLogLevel, level)
}

func (s *LoggerProviderTestSuite) TestRegisterCustomLevel() {
	os.Setenv(levelEnvName, "debug")
	defer os.Unsetenv(levelEnvName)

	s.provider.Register(s.container)

	level, err := s.container.Get("logger.level")
	s.Require().NoError(err)
	s.Require().Equal(logrus.DebugLevel, level)
}

func (s *LoggerProviderTestSuite) TestRegisterIncorrectCustomTimeout() {
	incorrectLevel := "debug_test"

	os.Setenv(levelEnvName, incorrectLevel)
	defer os.Unsetenv(levelEnvName)

	s.provider.Register(s.container)

	level, err := s.container.Get("logger.level")
	s.Require().NoError(err)
	s.Require().Equal(defaultLogLevel, level)
}

func (s *LoggerProviderTestSuite) TestRegisterFormat() {
	s.provider.Register(s.container)

	formatter, err := s.container.Get("logger.format")
	s.Require().NoError(err)
	s.Require().Equal(&logrus.TextFormatter{}, formatter)
}

func (s *LoggerProviderTestSuite) TestRegisterJsonFormat() {
	os.Setenv(formatEnvName, "json")
	defer os.Unsetenv(formatEnvName)

	s.provider.Register(s.container)

	formatter, err := s.container.Get("logger.format")
	s.Require().NoError(err)
	s.Require().Equal(&logrus.JSONFormatter{}, formatter)
}

func (s *LoggerProviderTestSuite) TestRegisterIncorrectCustomFormat() {
	incorrectFormat := "xml"

	os.Setenv(formatEnvName, incorrectFormat)
	defer os.Unsetenv(formatEnvName)

	s.provider.Register(s.container)

	formatter, err := s.container.Get("logger.format")
	s.Require().NoError(err)
	s.Require().Equal(&logrus.TextFormatter{}, formatter)
}

func (s *LoggerProviderTestSuite) TestLoggerService() {
	s.provider.Register(s.container)

	logger, err := s.container.Get("logger")

	s.Require().NoError(err, "It should provide logger service")
	s.Require().Implements((*logrus.FieldLogger)(nil), logger, "Logger should implement Logger interface")
}

func TestLoggerProviderTestSuite(t *testing.T) {
	suite.Run(t, new(LoggerProviderTestSuite))
}

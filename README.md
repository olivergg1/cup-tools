# Tools for cup DI container (GoLang projects)

## Available tools

### cache
Uses <a href="//github.com/allegro/bigcache">github.com/allegro/bigcache</a>
Environment variables:
* ```CACHE_SIZE``` - max cache size in megabytes. By default - *128*
* ```CACHE_TTL``` - TTL for each cache kv. By default - *5m*. Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".

### consul
Uses <a href="//github.com/hashicorp/consul">github.com/hashicorp/consul</a>
Environment variables:
* ```CONSUL_HTTP_ADDR``` - consul agent address. By default - *127.0.0.1:8500*

### grpc
Uses <a href="//google.golang.org/grpc">google.golang.org/grpc</a>

### http
Uses <a href="//github.com/valyala/fasthttp">github.com/valyala/fasthttp</a>
Enviroment variables:
* ```HTTP_SERVER_ADDR``` - listening address

### logger
Uses <a href="//github.com/sirupsen/logrus">github.com/sirupsen/logrus</a>
Enviroment variables:
* ```LOG_LEVEL``` - available options: *panic*, *fatal*, *error*, *warn*, *warning*, *info*, *debug*. By default - *info*
* ```LOG_FORMAT``` - available options: *json*, *text*. By default - *text*

### metrics
Uses <a href="//github.com/prometheus/client_golang/prometheus">github.com/prometheus/client_golang/prometheus</a>
Open */metrics* endpoint with metrics for prometheus

### middleware
Metrics middleware for HTTP requests

### sd
Service discovery. Uses *consul* tool for register/search/etc services in consul.
Enviroment variables:
* ```SD_DC``` - datacenter name. By default - *dc1*
* ```SD_CLUSTER``` - cluster name. By default - *dev*


package grpc

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"net"
	"os"
)

type Server interface {
	GracefulStop()
	RegisterService(sd *grpc.ServiceDesc, ss interface{})
	Serve(lis net.Listener) error
}

const serverAddrEnvName = "GRPC_SERVER_ADDR"

// gRPC server provider
type Provider struct {
	addr string
}

func NewProvider(addr string) *Provider {
	return &Provider{addr: addr}
}

// Register components related to gRPC server in service container
func (p *Provider) Register(c cup.Container) {
	p.registerAddr(c)
	p.registerListener(c)
	p.registerMiddlewares(c)
	p.registerServer(c)
}

// Starting gRPC server
func (p *Provider) Boot(c cup.Container) error {
	grpc_prometheus.EnableHandlingTimeHistogram()

	listener := c.MustGet("grpc.listener").(net.Listener)
	server := c.MustGet("grpc.server").(Server)

	go func() {
		server.Serve(listener)
	}()

	return nil
}

// Shutdown gRPC server
func (p *Provider) Shutdown(c cup.Container) {
	server := c.MustGet("grpc.server").(Server)
	server.GracefulStop()
}

// Register GRPC server addr in service container
func (p *Provider) registerAddr(c cup.Container) {
	c.Set("grpc.addr", func(c cup.Container) interface{} {
		addr, exist := os.LookupEnv(serverAddrEnvName)
		if exist {
			return addr
		}

		return p.addr
	})
}

// Register network listener as a service
func (p *Provider) registerListener(c cup.Container) {
	c.Set("grpc.listener", func(c cup.Container) interface{} {
		addr := c.MustGet("grpc.addr").(string)

		listener, err := net.Listen("tcp", addr)
		if err != nil {
			panic(err)
		}

		return listener
	})
}

// Register gRPC server as a service
func (p *Provider) registerServer(c cup.Container) {
	c.Set("grpc.server", func(c cup.Container) interface{} {
		middlewares := c.MustGet("grpc.middlewares").([]grpc.UnaryServerInterceptor)

		return grpc.NewServer(grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(middlewares...)))
	})
}

// Register gRPC server middlewares
// By default register only logging middleware
func (p *Provider) registerMiddlewares(c cup.Container) {
	c.Set("grpc.middlewares", func(c cup.Container) interface{} {
		log := c.MustGet("logger").(*logrus.Logger)
		logEntry := logrus.NewEntry(log)
		grpc_logrus.ReplaceGrpcLogger(logEntry)

		return []grpc.UnaryServerInterceptor{
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_logrus.UnaryServerInterceptor(logEntry),
		}
	})
}

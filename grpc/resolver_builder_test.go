package grpc

import (
	"fmt"
	"bitbucket.org/kovalevm/tools/mocks"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc/resolver"
	"strings"
	"testing"
)

type ResolverBuilderTestSuite struct {
	suite.Suite
	registry *mocks.Registry
	cc       *testClientConn
	builder  resolver.Builder
}

func (s *ResolverBuilderTestSuite) SetupTest() {
	s.registry = new(mocks.Registry)
	s.cc = new(testClientConn)
	s.builder = NewResolverBuilder(s.registry)
}

func (s *ResolverBuilderTestSuite) TestScheme() {
	s.Require().Equal("registry", s.builder.Scheme())
}

func (s *ResolverBuilderTestSuite) TestBuild() {
	service := "example-service"
	tags := []string{"test", "v1"}
	nodes := []string{"127.0.0.1:1080"}

	s.registry.On("Get", mock.Anything, service, tags, mock.Anything).Return(nodes, uint64(0), nil)

	expAddrs := []resolver.Address{{Addr: "127.0.0.1:1080", Type: resolver.Backend}}
	s.cc.On("NewAddress", expAddrs)

	target := resolver.Target{
		Endpoint: fmt.Sprintf("%s,%s", service, strings.Join(tags, ",")),
	}
	opts := resolver.BuildOption{}

	res, err := s.builder.Build(target, s.cc, opts)

	s.Require().NoError(err)
	s.Require().Implements((*resolver.Resolver)(nil), res)
	s.registry.AssertExpectations(s.T())
	s.cc.AssertExpectations(s.T())

	res.Close()
}

func TestResolverBuilderTestSuite(t *testing.T) {
	suite.Run(t, new(ResolverBuilderTestSuite))
}

package grpc

import (
	"bitbucket.org/kovalevm/tools/sd"
	"google.golang.org/grpc/resolver"
	"strings"
)

type resolverBuilder struct {
	registry sd.Registry
}

func NewResolverBuilder(registry sd.Registry) resolver.Builder {
	return &resolverBuilder{registry: registry}
}

func (b *resolverBuilder) Build(target resolver.Target, cc resolver.ClientConn, opts resolver.BuildOption) (resolver.Resolver, error) {
	service, tags := b.splitEndpoint(target.Endpoint)

	res := NewResolver(b.registry, service, tags, cc)
	err := res.Init()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (b *resolverBuilder) Scheme() string {
	return "registry"
}

func (b *resolverBuilder) splitEndpoint(endpoint string) (service string, tags []string) {
	parts := strings.Split(endpoint, ",")
	service = parts[0]
	if len(parts) > 1 {
		tags = parts[1:]
	}

	return
}

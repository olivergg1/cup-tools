package grpc

import (
	"bitbucket.org/kovalevm/cup"
	"bitbucket.org/kovalevm/tools/mocks"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/suite"
	"net"
	"os"
	"testing"
	"time"
)

type GrpcProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider  *Provider
}

func (s *GrpcProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()

	s.provider = NewProvider(":0")
}

func (s *GrpcProviderTestSuite) TestRegisterAddr() {
	s.provider.Register(s.container)

	addr, err := s.container.Get("grpc.addr")
	s.Require().NoError(err)
	s.Require().Equal(s.provider.addr, addr)
}

func (s *GrpcProviderTestSuite) TestRegisterCustomAddr() {
	expAddr := "0.0.0.0:8091"

	os.Setenv(serverAddrEnvName, expAddr)
	defer os.Unsetenv(serverAddrEnvName)

	s.provider.Register(s.container)

	addr, err := s.container.Get("grpc.addr")
	s.Require().NoError(err)
	s.Require().Equal(expAddr, addr)
}

func (s *GrpcProviderTestSuite) TestRegisterListener() {
	s.provider.Register(s.container)

	listener, err := s.container.Get("grpc.listener")
	s.Require().NoError(err)
	s.Require().Implements((*net.Listener)(nil), listener)
}

func (s *GrpcProviderTestSuite) TestRegisterServer() {
	s.container.Set("logger", logrus.New())
	s.provider.Register(s.container)

	server, err := s.container.Get("grpc.server")
	s.Require().NoError(err)
	s.Require().Implements((*Server)(nil), server)
}

func (s *GrpcProviderTestSuite) TestBoot() {
	container := new(mocks.Container)

	listener := s.expectListenerAcquired(container)
	server := s.expectServerAcquired(container)

	server.On("Serve", listener).Return(nil).Once()

	s.provider.Boot(container)

	time.Sleep(1 * time.Second)

	container.AssertExpectations(s.T())
	server.AssertExpectations(s.T())
}

func (s *GrpcProviderTestSuite) TestShutdown() {
	container := new(mocks.Container)

	server := s.expectServerAcquired(container)
	server.On("GracefulStop").Once()

	s.provider.Shutdown(container)

	container.AssertExpectations(s.T())
	server.AssertExpectations(s.T())
}

func (s *GrpcProviderTestSuite) expectLoggerAcquired(container *mocks.Container) *test.Hook {
	logger, hook := test.NewNullLogger()
	container.On("MustGet", "logger").Return(logger).Once()

	return hook
}

func (s *GrpcProviderTestSuite) expectServerAcquired(container *mocks.Container) *mocks.GrpcServer {
	server := new(mocks.GrpcServer)
	container.On("MustGet", "grpc.server").Return(server).Once()

	return server
}

func (s *GrpcProviderTestSuite) expectListenerAcquired(container *mocks.Container) *mocks.NetListener {
	listener := new(mocks.NetListener)
	container.On("MustGet", "grpc.listener").Return(listener).Once()

	return listener
}

func TestGrpcProviderTestSuite(t *testing.T) {
	suite.Run(t, new(GrpcProviderTestSuite))
}

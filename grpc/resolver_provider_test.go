package grpc

import (
	"bitbucket.org/kovalevm/cup"
	"bitbucket.org/kovalevm/tools/mocks"
	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc/resolver"
	"testing"
)

type ResolverProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider  *ResolverProvider
}

func (s *ResolverProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()
	s.container.Set("sd.registry", new(mocks.Registry))

	s.provider = new(ResolverProvider)
}

func (s *ResolverProviderTestSuite) TestBoot() {
	err := s.provider.Boot(s.container)
	s.Require().NoError(err)

	builder := resolver.Get("registry")
	s.Require().IsType((*resolverBuilder)(nil), builder)
}

func TestResolverProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ResolverProviderTestSuite))
}

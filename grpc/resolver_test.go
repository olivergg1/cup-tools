package grpc

import (
	"bitbucket.org/kovalevm/tools/mocks"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc/resolver"
	"testing"
)

type testClientConn struct {
	mock.Mock
}

func (c *testClientConn) NewAddress(addresses []resolver.Address) {
	c.Called(addresses)
}

func (c *testClientConn) NewServiceConfig(serviceConfig string) {
	c.Called(serviceConfig)
}

type RegistryResolverTestSuite struct {
	suite.Suite
	cc       *testClientConn
	registry *mocks.Registry
}

func (s *RegistryResolverTestSuite) SetupTest() {
	s.cc = new(testClientConn)
	s.registry = new(mocks.Registry)
}

func (s *RegistryResolverTestSuite) TestInit() {
	service := "testsvc"
	tags := []string{"test", "v1"}

	nodes := []string{"127.0.0.1:1080", "127.0.0.1:1085"}
	s.registry.On("Get", mock.Anything, service, tags, mock.Anything).Return(nodes, uint64(0), nil)

	addrs := []resolver.Address{
		{Addr: "127.0.0.1:1080", Type: resolver.Backend},
		{Addr: "127.0.0.1:1085", Type: resolver.Backend},
	}
	s.cc.On("NewAddress", addrs)

	res := NewResolver(s.registry, service, tags, s.cc)
	err := res.Init()

	s.Require().NoError(err)
	s.registry.AssertExpectations(s.T())
	s.cc.AssertExpectations(s.T())

	res.Close()
}

func (s *RegistryResolverTestSuite) TestResolveNow() {
	service := "testsvc"
	tags := []string{"test", "v1"}

	nodes := []string{"127.0.0.1:1080", "127.0.0.1:1085"}
	s.registry.On("Get", mock.Anything, service, tags, mock.Anything).Return(nodes, uint64(0), nil)

	addrs := []resolver.Address{
		{Addr: "127.0.0.1:1080", Type: resolver.Backend},
		{Addr: "127.0.0.1:1085", Type: resolver.Backend},
	}
	s.cc.On("NewAddress", addrs)

	res := NewResolver(s.registry, service, tags, s.cc)
	res.ResolveNow(resolver.ResolveNowOption{})

	s.registry.AssertExpectations(s.T())
	s.cc.AssertExpectations(s.T())

	res.Close()
}

func TestConsulResolverTestSuite(t *testing.T) {
	suite.Run(t, new(RegistryResolverTestSuite))
}

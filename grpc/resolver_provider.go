package grpc

import (
	"bitbucket.org/kovalevm/cup"
	"bitbucket.org/kovalevm/tools/sd"
	"google.golang.org/grpc/resolver"
)

type ResolverProvider struct{}

func (p *ResolverProvider) Boot(c cup.Container) error {
	registry := c.MustGet("sd.registry").(sd.Registry)
	resolver.Register(NewResolverBuilder(registry))

	return nil
}

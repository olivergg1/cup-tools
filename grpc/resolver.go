package grpc

import (
	"context"
	"bitbucket.org/kovalevm/tools/sd"
	"google.golang.org/grpc/resolver"
	"sync"
)

// Naming resolver that use service registry for service discovery
//
// To use this resolver you need specify connection string in the following format:
//
// registry://<service_name>[,tags]
//
// For example, registry://test-service,prod,v1
type RegistryResolver struct {
	registry sd.Registry
	cc       resolver.ClientConn
	service  string
	tags     []string
	ctx      context.Context
	cancel   context.CancelFunc
	wg       sync.WaitGroup
	waitIdx  uint64
}

// Creates naming resolver that use service registry for service discovery
func NewResolver(registry sd.Registry, service string, tags []string, cc resolver.ClientConn) *RegistryResolver {
	ctx, cancel := context.WithCancel(context.Background())

	return &RegistryResolver{
		registry: registry,
		cc:       cc,
		service:  service,
		tags:     tags,
		ctx:      ctx,
		cancel:   cancel,
	}
}

func (r *RegistryResolver) Init() error {
	err := r.lookup(true)
	if err != nil {
		return err
	}

	r.wg.Add(1)
	go r.watcher()

	return nil
}

func (r *RegistryResolver) ResolveNow(option resolver.ResolveNowOption) {
	_ = r.lookup(true)
}

func (r *RegistryResolver) Close() {
	r.cancel()
	r.wg.Wait()
}

func (r *RegistryResolver) watcher() {
	defer r.wg.Done()
	for {
		select {
		case <-r.ctx.Done():
			return
		default:
			_ = r.lookup(false)
		}
	}
}

func (r *RegistryResolver) lookup(now bool) error {
	var waitIdx uint64
	if !now {
		waitIdx = r.waitIdx
	}

	nodes, waitIdx, err := r.registry.Get(r.ctx, r.service, r.tags, waitIdx)
	if err != nil {
		return err
	}

	r.waitIdx = waitIdx

	addrs := make([]resolver.Address, len(nodes))
	for idx, node := range nodes {
		addrs[idx] = resolver.Address{Addr: node, Type: resolver.Backend}
	}

	r.cc.NewAddress(addrs)

	return nil
}

package metrics

import (
	"bitbucket.org/kovalevm/cup"
	"google.golang.org/grpc"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
	"github.com/buaazp/fasthttprouter"
)

// Service provider that register components required for metrics collection
//
// It's register:
//    - middleware that collect metrics for gRPC server calls
//    - HTTP route that report collected metrics
type Provider struct {}

// Register components in service container
func (p *Provider) Register(c cup.Container) {
	p.registerMiddleware(c)
	p.registerHttpRoute(c)
}

func (p *Provider) registerMiddleware(c cup.Container) {
	c.MustExtend("grpc.middlewares", func(old interface{}, c cup.Container) interface{} {
		middlewares := old.([]grpc.UnaryServerInterceptor)
		middlewares = append(middlewares, grpc_prometheus.UnaryServerInterceptor)

		return middlewares
	})
}

func (p *Provider) registerHttpRoute(c cup.Container) {
	c.MustExtend("http.router", func(old interface{}, c cup.Container) interface{} {
		routes := old.(*fasthttprouter.Router)
		routes.GET("/metrics", fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler()))

		return routes
	})
}

package metrics

import (
	"github.com/stretchr/testify/suite"
	"bitbucket.org/kovalevm/cup"
	"testing"
	"google.golang.org/grpc"
	"github.com/buaazp/fasthttprouter"
)

type ProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider *Provider
}

func (s *ProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()

	s.container.Set("grpc.middlewares", func(c cup.Container) interface{} {
		return []grpc.UnaryServerInterceptor{}
	})

	s.container.Set("http.router", func(c cup.Container) interface{} {
		return fasthttprouter.New()
	})

	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegisterMiddleware() {
	s.provider.Register(s.container)

	middlewares := s.container.MustGet("grpc.middlewares").([]grpc.UnaryServerInterceptor)

	s.Require().Equal(1, len(middlewares))
}

func (s *ProviderTestSuite) TestRegisterRoute() {
	s.provider.Register(s.container)

	routes := s.container.MustGet("http.router").(*fasthttprouter.Router)

	handler, _ := routes.Lookup("GET", "/metrics", nil)

	s.Require().NotNil(handler)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}
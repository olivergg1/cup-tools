package mocks

import "github.com/stretchr/testify/mock"

type Cache struct {
	mock.Mock
}

func (c *Cache) Get(key string) ([]byte, error) {
	args := c.Called(key)

	return args.Get(0).([]byte), args.Error(1)
}

func (c *Cache) Set(key string, data []byte) error {
	args := c.Called(key, data)

	return args.Error(0)
}

func (c *Cache) Reset() error {
	args := c.Called()

	return args.Error(0)
}

func (c *Cache) Len() int {
	args := c.Called()

	return args.Int(0)
}
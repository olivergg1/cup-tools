package mocks

import (
	"context"
	"github.com/stretchr/testify/mock"
)

type Registry struct {
	mock.Mock
}

func (r *Registry) Get(ctx context.Context, service string, tags []string, waitIdx uint64) ([]string, uint64, error) {
	args := r.Called(ctx, service, tags, waitIdx)

	return args.Get(0).([]string), args.Get(1).(uint64), args.Error(2)
}

package mocks

import (
	"github.com/stretchr/testify/mock"
	"bitbucket.org/kovalevm/cup"
)

type Container struct {
	mock.Mock
}

func (c *Container) Set(key string, val interface{}) {
	c.Called(key, val)
}

func (c *Container) Factory(key string, fn func(c cup.Container) interface{}) {
	c.Called(key, fn)
}

func (c *Container) Protect(key string, fn func(c cup.Container) interface{}) {
	c.Called(key, fn)
}

func (c *Container) Has(key string) bool {
	args := c.Called(key)

	return args.Bool(0)
}

func (c *Container) Get(key string) (interface{}, error) {
	args := c.Called(key)

	return args.Get(0), args.Error(1)
}

func (c *Container) MustGet(key string) interface{} {
	args := c.Called(key)

	return args.Get(0)
}

func (c *Container) Extend(key string, fn cup.ExtendFn) error {
	args := c.Called(key, fn)

	return args.Error(0)
}

func (c *Container) MustExtend(key string, fn cup.ExtendFn) {
	c.Called(key, fn)
}

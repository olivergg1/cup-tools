package mocks

import (
	"github.com/stretchr/testify/mock"
	"net"
	"google.golang.org/grpc"
)

type GrpcServer struct {
	mock.Mock
}

func (s *GrpcServer) RegisterService(sd *grpc.ServiceDesc, ss interface{}) {
	s.Called(sd, ss)
}

func (s *GrpcServer) Serve(lis net.Listener) error {
	args := s.Called(lis)

	return args.Error(0)
}

func (s *GrpcServer) GracefulStop() {
	s.Called()
}
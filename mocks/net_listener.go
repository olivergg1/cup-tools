package mocks

import (
	"github.com/stretchr/testify/mock"
	"net"
)

type NetListener struct {
	mock.Mock
}

func (l *NetListener) Accept() (net.Conn, error) {
	args := l.Called()

	return args.Get(0).(net.Conn), args.Error(1)
}

func (l *NetListener) Close() error {
	args := l.Called()

	return args.Error(0)
}

func (l *NetListener) Addr() net.Addr {
	args := l.Called()

	return args.Get(0).(net.Addr)
}
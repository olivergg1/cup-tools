package debug

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/stretchr/testify/suite"
	"os"
	"testing"
)

type ProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider  *Provider
}

func (s *ProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()
	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegisterDebug() {
	s.provider.Register(s.container)

	debug, err := s.container.Get("debug")
	s.Require().NoError(err)
	s.Require().Equal(false, debug)
}

func (s *ProviderTestSuite) TestRegisterCustomDebug() {
	os.Setenv(debugEnvName,  "custom1")
	defer os.Unsetenv(debugEnvName)

	s.provider.Register(s.container)

	debug, err := s.container.Get("debug")
	s.Require().NoError(err)
	s.Require().Equal(false, debug)
}

func (s *ProviderTestSuite) TestRegister1Debug() {
	os.Setenv(debugEnvName, "1")
	defer os.Unsetenv(debugEnvName)

	s.provider.Register(s.container)

	debug, err := s.container.Get("debug")
	s.Require().NoError(err)
	s.Require().Equal(true, debug)
}

func (s *ProviderTestSuite) TestRegisterTrueDebug() {
	os.Setenv(debugEnvName, "true")
	defer os.Unsetenv(debugEnvName)

	s.provider.Register(s.container)

	debug, err := s.container.Get("debug")
	s.Require().NoError(err)
	s.Require().Equal(true, debug)
}

func (s *ProviderTestSuite) TestRegisterFalseDebug() {
	os.Setenv(debugEnvName, "false")
	defer os.Unsetenv(debugEnvName)

	s.provider.Register(s.container)

	debug, err := s.container.Get("debug")
	s.Require().NoError(err)
	s.Require().Equal(false, debug)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}

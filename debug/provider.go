package debug

import (
	"bitbucket.org/kovalevm/cup"
	"os"
)

const (
	debugEnvName = "DEBUG"
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	c.Set("debug", func(c cup.Container) interface{} {
		d, exist := os.LookupEnv(debugEnvName)
		if exist && d != "" {
			return d == "1" || d == "true" || d == "TRUE"
		}

		return false
	})
}

package middleware

import (
	"bitbucket.org/kovalevm/tools/http"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"time"
)

func Logger(logger logrus.FieldLogger) http.Middleware {
	return func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			startedAt := time.Now()

			next(ctx)

			fields := logrus.Fields{
				"http.code":     ctx.Response.StatusCode(),
				"http.time_ms":  int64(time.Since(startedAt) / time.Millisecond),
				"http.method":   string(ctx.Request.Header.Method()),
				"http.endpoint": string(ctx.Request.URI().Path()),
			}
			logger.WithFields(fields).Info("finished call")
		}
	}
}

package cache

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/allegro/bigcache"
	"github.com/sirupsen/logrus"
	"os"
	"strconv"
	"time"
)

// Data cache
type Cache interface {
	// Returns cache value for specified key
	Get(string) ([]byte, error)

	// Sets cache value for specified key
	Set(string, []byte) error

	// Clears cache
	Reset() error

	// Returns number of entries in the cache
	Len() int
}

const sizeEnvName = "CACHE_SIZE"
const ttlEnvName = "CACHE_TTL"
const defaultSize = 128
const defaultTtl = 5 * time.Minute

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerCacheSize(c)
	p.registerCacheTtl(c)
	p.registerCache(c)
}

func (p *Provider) registerCacheSize(c cup.Container) {
	c.Set("cache.size", func(c cup.Container) interface{} {
		sizeStr, exist := os.LookupEnv(sizeEnvName)
		if exist {
			size, err := strconv.Atoi(sizeStr)
			if err != nil {
				logger := c.MustGet("logger").(logrus.FieldLogger)
				logger.Errorf("Invalid cache size: %s", sizeStr)
			} else {
				return size
			}
		}

		return defaultSize
	})
}

func (p *Provider) registerCacheTtl(c cup.Container) {
	c.Set("cache.ttl", func(c cup.Container) interface{} {
		ttlStr, exist := os.LookupEnv(ttlEnvName)
		if exist {
			ttl, err := time.ParseDuration(ttlStr)
			if err != nil {
				logger := c.MustGet("logger").(logrus.FieldLogger)
				logger.Errorf("Invalid cache TTL: %s", ttlStr)
			} else {
				return ttl
			}
		}

		return defaultTtl
	})
}

func (p *Provider) registerCache(c cup.Container) {
	c.Set("cache", func(c cup.Container) interface{} {
		maxSize := c.MustGet("cache.size").(int)
		ttl := c.MustGet("cache.ttl").(time.Duration)

		conf := bigcache.DefaultConfig(ttl)
		conf.HardMaxCacheSize = maxSize
		conf.CleanWindow = time.Minute

		cache, err := bigcache.NewBigCache(conf)
		if err != nil {
			panic(err)
		}

		return cache
	})
}

package cache

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/suite"
	"os"
	"strconv"
	"testing"
	"time"
)

type ProviderTestSuite struct {
	suite.Suite
	logger     logrus.FieldLogger
	loggerHook *test.Hook
	container  cup.Container
	provider   *Provider
}

func (s *ProviderTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()

	s.container = cup.NewApp()
	s.container.Set("logger", s.logger)

	s.provider = new(Provider)
}

func (s *ProviderTestSuite) TestRegisterSize() {
	s.provider.Register(s.container)

	size, err := s.container.Get("cache.size")
	s.Require().NoError(err)
	s.Require().Equal(defaultSize, size)
}

func (s *ProviderTestSuite) TestRegisterCustomSize() {
	expSize := 64

	os.Setenv(sizeEnvName, strconv.Itoa(expSize))
	defer os.Unsetenv(sizeEnvName)

	s.provider.Register(s.container)

	size, err := s.container.Get("cache.size")
	s.Require().NoError(err)
	s.Require().Equal(expSize, size)
}

func (s *ProviderTestSuite) TestRegisterInvalidCustomSize() {
	os.Setenv(sizeEnvName, "invalid")
	defer os.Unsetenv(sizeEnvName)

	s.provider.Register(s.container)

	size, err := s.container.Get("cache.size")
	s.Require().NoError(err)
	s.Require().Equal(defaultSize, size)
	s.Require().Equal(logrus.ErrorLevel, s.loggerHook.LastEntry().Level)
	s.Require().Equal("Invalid cache size: invalid", s.loggerHook.LastEntry().Message)
}

func (s *ProviderTestSuite) TestRegisterTtl() {
	s.provider.Register(s.container)

	ttl, err := s.container.Get("cache.ttl")
	s.Require().NoError(err)
	s.Require().Equal(defaultTtl, ttl)
}

func (s *ProviderTestSuite) TestRegisterCustomTtl() {
	expTtl := 1 * time.Minute

	os.Setenv(ttlEnvName, expTtl.String())
	defer os.Unsetenv(ttlEnvName)

	s.provider.Register(s.container)

	ttl, err := s.container.Get("cache.ttl")
	s.Require().NoError(err)
	s.Require().Equal(expTtl, ttl)
}

func (s *ProviderTestSuite) TestRegisterInvalidCustomTtl() {
	os.Setenv(ttlEnvName, "invalid")
	defer os.Unsetenv(ttlEnvName)

	s.provider.Register(s.container)

	ttl, err := s.container.Get("cache.ttl")
	s.Require().NoError(err)
	s.Require().Equal(defaultTtl, ttl)
	s.Require().Equal(logrus.ErrorLevel, s.loggerHook.LastEntry().Level)
	s.Require().Equal("Invalid cache TTL: invalid", s.loggerHook.LastEntry().Message)
}

func (s *ProviderTestSuite) TestRegisterCache() {
	s.provider.Register(s.container)

	cache, err := s.container.Get("cache")
	s.Require().NoError(err)
	s.Require().Implements((*Cache)(nil), cache)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}

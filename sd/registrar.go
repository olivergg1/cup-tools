package sd

import (
	"context"
	"fmt"
	"bitbucket.org/kovalevm/cup"
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	"net"
	"os"
	"strconv"
	"sync"
	"time"
)

type Registrar struct {
	id     string
	name   string
	tags   []string
	addr   string
	ctx    context.Context
	cancel context.CancelFunc
	wg     sync.WaitGroup
}

func NewRegistrar(name string, tags []string, addr string) *Registrar {
	ctx, cancel := context.WithCancel(context.Background())
	return &Registrar{
		name:   name,
		tags:   tags,
		addr:   addr,
		ctx:    ctx,
		cancel: cancel,
	}
}

func (p *Registrar) Boot(c cup.Container) error {
	client := c.MustGet("consul.client").(*api.Client)

	sDef, err := p.getServiceDefinition(c)
	if err != nil {
		return nil
	}

	// register service
	err = client.Agent().ServiceRegister(sDef)
	if err != nil {
		return err
	}

	// register check
	cDef := p.getCheckDefinition()
	err = client.Agent().CheckRegister(cDef)
	if err != nil {
		return err
	}

	p.wg.Add(1)
	go p.loop(client)

	return nil
}

func (p *Registrar) Shutdown(c cup.Container) {
	p.cancel()
	p.wg.Wait()

	client := c.MustGet("consul.client").(*api.Client)
	logger := c.MustGet("logger").(logrus.FieldLogger)

	err := client.Agent().CheckDeregister(p.id)
	if err != nil {
		logger.Errorf("Failed to deregister service check: %s", err)
	}

	err = client.Agent().ServiceDeregister(p.id)
	if err != nil {
		logger.Errorf("Failed to deregister '%s' service: %s", p.name, err)
	}
}

func (p *Registrar) loop(client *api.Client) {
	defer p.wg.Done()
	for {
		select {
		case <-p.ctx.Done():
			return
		case <-time.After(time.Second * 5):
			client.Agent().UpdateTTL(p.id, "Ok", api.HealthPassing)
		}
	}
}

func (p *Registrar) getServiceDefinition(c cup.Container) (*api.AgentServiceRegistration, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}

	addr := c.MustGet(p.addr).(string)

	host, portStr, err := net.SplitHostPort(addr)
	if err != nil {
		return nil, err
	}
	port, err := strconv.Atoi(portStr)
	if err != nil {
		return nil, err
	}

	p.id = fmt.Sprintf("%s-%s-%d", p.name, hostname, port)

	def := &api.AgentServiceRegistration{
		ID:      p.id,
		Name:    p.name,
		Tags:    p.getTags(c),
		Address: host,
		Port:    port,
	}

	return def, nil
}

func (p *Registrar) getCheckDefinition() *api.AgentCheckRegistration {
	def := &api.AgentCheckRegistration{
		ID:        p.id,
		ServiceID: p.id,
		Name:      fmt.Sprintf("%s service check", p.name),
	}
	def.TTL = "10s"
	def.DeregisterCriticalServiceAfter = "1m"

	return def
}

func (p *Registrar) getTags(c cup.Container) []string {
	tags := p.tags

	cluster, err := c.Get("sd.cluster")
	if err == nil {
		tags = append(tags, cluster.(string))
	}

	return tags
}

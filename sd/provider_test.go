package sd

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/hashicorp/consul/api"
	"github.com/stretchr/testify/suite"
	"testing"
	"os"
)

type ProviderTestSuite struct {
	suite.Suite
	container cup.Container
	provider  *Provider
}

func (s *ProviderTestSuite) SetupTest() {
	s.container = cup.NewApp()
	s.provider = new(Provider)

	consul, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		s.T().Fatal("Couldn't create consul client: %s", err)
	}

	s.container.Set("consul.client", consul)
	s.container.Set("sd.datacenter", "test")
	s.container.Set("sd.cluster", "test")
}

func (s *ProviderTestSuite) TestRegisterRegistry() {
	s.provider.Register(s.container)

	registry, err := s.container.Get("sd.registry")
	s.Require().NoError(err)
	s.Require().Implements((*Registry)(nil), registry)
}

func (s *ProviderTestSuite) TestRegisterDatacenter() {
	s.provider.Register(s.container)

	dc, err := s.container.Get("sd.datacenter")
	s.Require().NoError(err)
	s.Require().Equal(defaultDatacenter, dc)
}

func (s *ProviderTestSuite) TestRegisterCustomDatacenter() {
	expDc := "custom1"

	os.Setenv(datacenterEnvName, expDc)
	defer os.Unsetenv(datacenterEnvName)

	s.provider.Register(s.container)

	dc, err := s.container.Get("sd.datacenter")
	s.Require().NoError(err)
	s.Require().Equal(expDc, dc)
}

func (s *ProviderTestSuite) TestRegisterCluster() {
	s.provider.Register(s.container)

	cluster, err := s.container.Get("sd.cluster")
	s.Require().NoError(err)
	s.Require().Equal(defaultCluster, cluster)
}

func (s *ProviderTestSuite) TestRegisterCustomCluster() {
	expCluster := "test"
	os.Setenv(clusterEnvName, expCluster)
	defer os.Unsetenv(clusterEnvName)

	s.provider.Register(s.container)

	cluster, err := s.container.Get("sd.cluster")
	s.Require().NoError(err)
	s.Require().Equal(expCluster, cluster)
}

func TestProviderTestSuite(t *testing.T) {
	suite.Run(t, new(ProviderTestSuite))
}

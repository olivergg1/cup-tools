package sd

import (
	"bitbucket.org/kovalevm/cup"
	"github.com/hashicorp/consul/api"
	"os"
)

const (
	datacenterEnvName = "SD_DC"
	clusterEnvName    = "SD_CLUSTER"
)

const (
	defaultDatacenter = "dc1"
	defaultCluster    = "dev"
)

type Provider struct{}

func (p *Provider) Register(c cup.Container) {
	p.registerDatacenter(c)
	p.registerCluster(c)
	p.registerRegistry(c)
}

func (p *Provider) registerRegistry(c cup.Container) {
	c.Set("sd.registry", func(c cup.Container) interface{} {
		consul := c.MustGet("consul.client").(*api.Client)
		dc := c.MustGet("sd.datacenter").(string)
		cluster := c.MustGet("sd.cluster").(string)

		return NewRegistry(consul, dc, cluster)
	})
}

func (p *Provider) registerDatacenter(c cup.Container) {
	c.Set("sd.datacenter", func(c cup.Container) interface{} {
		dc, exist := os.LookupEnv(datacenterEnvName)
		if exist && dc != "" {
			return dc
		}

		return defaultDatacenter
	})
}

func (p *Provider) registerCluster(c cup.Container) {
	c.Set("sd.cluster", func(c cup.Container) interface{} {
		cluster, exist := os.LookupEnv(clusterEnvName)
		if exist && cluster != "" {
			return cluster
		}

		return defaultCluster
	})
}

package sd

import (
	"fmt"
	"bitbucket.org/kovalevm/cup"
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/suite"
	"net"
	"os"
	"strconv"
	"testing"
)

type RegistrarTestSuite struct {
	suite.Suite
	container  cup.Container
	logger     logrus.FieldLogger
	loggerHook *test.Hook
}

func (s *RegistrarTestSuite) SetupTest() {
	s.logger, s.loggerHook = test.NewNullLogger()

	s.container = cup.NewApp()
	s.container.Set("logger", s.logger)

	consul, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		s.T().Fatalf("Couldn't create consul client: %s", err)
	}
	s.container.Set("consul.client", consul)
}

func (s *RegistrarTestSuite) TestRegistrar() {
	addr := "localhost:9005"
	name := "test-name"
	tags := []string{"test", "v1"}
	hostname, err := os.Hostname()
	if err != nil {
		s.T().Fatalf("Failed to get host name: %s", err)
	}

	s.container.Set("addr", addr)
	provider := NewRegistrar(name, tags, "addr")

	err = provider.Boot(s.container)
	s.Require().NoError(err)

	expHost, expPortStr, _ := net.SplitHostPort(addr)
	expPort, _ := strconv.Atoi(expPortStr)
	expId := fmt.Sprintf("%s-%s-%d", name, hostname, expPort)

	client := s.container.MustGet("consul.client").(*api.Client)

	services, _, err := client.Catalog().Service(name, tags[0], &api.QueryOptions{})
	s.Require().NoError(err)
	s.Require().Len(services, 1)
	s.Require().Equal(name, services[0].ServiceName)
	s.Require().Equal(expId, services[0].ServiceID)
	s.Require().Equal(expHost, services[0].ServiceAddress)
	s.Require().Equal(expPort, services[0].ServicePort)
	s.Require().Equal(tags, services[0].ServiceTags)

	checks, _, err := client.Health().Checks(name, &api.QueryOptions{})
	s.Require().NoError(err)
	s.Require().Len(checks, 1)
	s.Require().Equal(expId, checks[0].ServiceID)
	s.Require().Equal(name, checks[0].ServiceName)

	provider.Shutdown(s.container)

	services, _, err = client.Catalog().Service(name, tags[0], &api.QueryOptions{})
	s.Require().NoError(err)
	s.Require().Len(services, 0)

	checks, _, err = client.Health().Checks(name, &api.QueryOptions{})
	s.Require().NoError(err)
	s.Require().Len(checks, 0)
}

func TestRegistrarTestSuite(t *testing.T) {
	suite.Run(t, new(RegistrarTestSuite))
}
